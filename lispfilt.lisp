;; lispfilt - kernel filter for PNGs written in Common Lisp
;;
;; Copyright (c) 2016 Robert Cochran
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy of
;; this software and associated documentation files (the "Software"), to deal in
;; the Software without restriction, including without limitation the rights to
;; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
;; the Software, and to permit persons to whom the Software is furnished to do so,
;; subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
;; FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
;; COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
;; IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

;; Silence load messages from Quicklisp
(with-open-stream (*standard-output* (make-broadcast-stream))
  (ql:quickload "png")
  (ql:quickload "parse-opt"))

;;; Filter setup

(defvar *filters* (make-hash-table :test #'equal))

(defun add-filter (name filter)
  (setf (gethash name *filters*) filter))

; Box blur
(add-filter "blur"
	    (make-list 9 :initial-element 1/9))

(add-filter "gaussian"
	    (mapcar (lambda (x) (/ x 16))
		    '(1 2 1
		      2 4 2
		      1 2 1)))

(add-filter "sharpen"
	    '( 0 -1  0
	      -1  5 -1
	       0 -1  0))

(add-filter "unsharpen"
	    (mapcar (lambda (x) (/ x 256))
		    '(-1  -4  -6  -4 -1
		      -4 -16 -24 -16 -4
		      -6 -24 476 -24 -6
		      -4 -16 -24 -16 -4
		      -1  -4  -6  -4 -1)))

(add-filter "outline"
	    '(-1 -1 -1
	      -1  8 -1
	      -1 -1 -1))

(add-filter "emboss"
	    '(-2 -1 0
	      -1  1 1
	       0  1 2))

(add-filter "topsobel"
	    '( 1  2  1
	       0  0  0
	      -1 -2 -1))

(add-filter "bottomsobel"
	    '(-1 -2 -1
	       0  0  0
	       1  2  1))

(add-filter "rightsobel"
	    '(-1 0 1
	      -2 0 2
	      -1 0 1))

(add-filter "leftsobel"
	    '(1 0 -1
	      2 0 -2
	      1 0 -1))

; Argument parsing specifications
(defvar *argv-option-specs*
  '(("i" "input" :required)
    ("o" "output" :required)
    ("b" "blur" :none)
    ("g" "gaussian" :none)
    ("s" "sharpen" :none)
    ("u" "unsharpen" :none)
    ("l" "outline" :none)
    ("e" "emboss" :none)
    (nil "topsobel" :none)
    (nil "bottomsobel" :none)
    (nil "rightsobel" :none)
    (nil "leftsobel" :none)))

;; Code

(defun wrap-int (x low high)
  (let* ((range (- high low))
	 (x (mod (- x low) range)))
    (+ x (if (minusp x)
	     high
	     low))))

(defun number->ubyte-8 (x)
  (coerce (truncate (max 0 (min x 255))) '(unsigned-byte 8)))

(defun filter-image-at-pixel (filter image y x)
  (let ((delta-max (truncate (/ (sqrt (length filter))
				2)))
	(w (png:image-width image))
	(h (png:image-height image))
	(filter-pos 0))
    (reduce (lambda (x y) (map 'list #'+ x y))
	    (reduce #'nconc
		    (loop for dy from (- delta-max) to delta-max collecting
			 (loop for dx from (- delta-max) to delta-max
			    collecting
			      (loop for i below 3 collecting
				   (* (aref image
					    (wrap-int (+ y dy) 0 h)
					    (wrap-int (+ x dx) 0 w)
					    i)
				      (aref filter
					    filter-pos)))
			    do (incf filter-pos)))))))

(defun filter-image (filter infile outfile)
  (let* ((orig (with-open-file (in infile :element-type '(unsigned-byte 8))
		 (png:decode in)))
	 (filtered (png:make-image (png:image-height orig)
				   (png:image-width orig)
				   (png:image-channels orig)))
	 (filter (make-array (length filter) :initial-contents filter)))
    (dotimes (y (png:image-height orig))
      (dotimes (x (png:image-width orig))
	(destructuring-bind (r g b) (filter-image-at-pixel filter orig y x)
	  (setf (aref filtered y x 0) (number->ubyte-8 r)
		(aref filtered y x 1) (number->ubyte-8 g)
		(aref filtered y x 2) (number->ubyte-8 b)))))
    (with-open-file (out outfile
			 :element-type '(unsigned-byte 8)
			 :direction :output
			 :if-exists :supersede)
      (png:encode filtered out))))

(defun get-argv ()
  (or
   #+sbcl (cdr sb-ext:*posix-argv*)
   #+ecl ext:argv
   nil))

(defun parse-argv ()
  (let ((options (parse-opt:parse-opt (get-argv) *argv-option-specs*))
	filter infile outfile)
    (dolist (opt (first options))
      (case (intern (string-upcase (car opt)) 'keyword)
	(:input (setf infile (cdr opt)))
	(:output (setf outfile (cdr opt)))
	; It's a built-in filter option
	(otherwise (setf filter (car opt)))))
    (if (and (every #'null (list infile outfile))
	     (<= 2 (length (second options))))
	(setf infile (first (second options))
	      outfile (second (second options))))
    (if (some #'null (list infile outfile))
	(error "Input or output file missing.~%"))
    (list (gethash filter *filters*) infile outfile)))

(apply #'filter-image (parse-argv))
